//
//  ContentView.swift
//  WatchLandmarks Extension
//
//  Created by Yuliia Karakai on 2021-05-12.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        LandmarkList()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(ModelData())
    }
}
