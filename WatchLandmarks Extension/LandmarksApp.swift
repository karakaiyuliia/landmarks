//
//  LandmarksApp.swift
//  WatchLandmarks Extension
//
//  Created by Yuliia Karakai on 2021-05-12.
//

import SwiftUI

@main
struct LandmarksApp: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
